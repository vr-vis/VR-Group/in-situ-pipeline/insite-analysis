import io
import json

from flask import current_app
import time
import connexion
import orjson



def get_analysis_communicator():
    return current_app.config['AnalysisCommunicator']


def get_test():
    AC = get_analysis_communicator()
    
    data = 5
    AC.send_data(data)
    data = AC.get_data()
    print(f"Got Data back: {data}")

    return data, 200


def get_short_desc(plugin_id):
    AC = get_analysis_communicator()
    response = AC.call_plugin_function(plugin_id, "get_short_description")
    
    return response, 200


def get_long_desc(plugin_id):
    AC = get_analysis_communicator()
    response = AC.call_plugin_function(plugin_id, "get_long_description")

    return response, 200


def get_plugin_info():
    AC = get_analysis_communicator()
    AC.send_data([], dest=1, tag=101)
    response = AC.get_data()

    return response, 200


def get_data(plugin_id):
    AC = get_analysis_communicator()
    additional_params = connexion.request.args
    if additional_params:
        response = AC.call_plugin_function(plugin_id, "get_data", additional_params)
    else:
        response = AC.call_plugin_function(plugin_id, "get_data")

    return orjson.dumps(response,option=orjson.OPT_SERIALIZE_NUMPY), 200


def get_params(plugin_id):
    AC = get_analysis_communicator()
    response = AC.call_plugin_function(plugin_id, "get_plugin_param_values")

    return response, 200


def put_params(plugin_id):
    body = connexion.request.args
    AC = get_analysis_communicator()
    response = AC.call_plugin_function(plugin_id, "set_param", body)
    
    return response, 200


def get_active(plugin_id):
    AC = get_analysis_communicator()
    response = AC.call_plugin_function(plugin_id, "get_plugin_activated")
    
    return response, 200


def put_active(plugin_id, new_state):
    AC = get_analysis_communicator()
    response = AC.call_plugin_function(plugin_id, "set_plugin_activated", new_state)
    
    return response


def get_plugin_details(plugin_id):
    AC = get_analysis_communicator()
    response = AC.call_plugin_function(plugin_id, "get_plugin_details")
    
    return response
