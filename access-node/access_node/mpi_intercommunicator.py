from mpi4py import MPI
import logging
import os
import time
import psycopg2

logger = logging.getLogger('root')
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)35s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)


class AnalysisCommunicator():
    intercommunicator = None
    mpi_world = MPI.COMM_WORLD

    def __init__(self):
        pass

    def get_port_from_db(self):
        database_host = 'database'
        con = psycopg2.connect(database='postgres', user='postgres', password='postgres', host=database_host,
                               port='5432')
        cur = con.cursor()
        cur.execute("SELECT port FROM mpi_port_information WHERE port_type = 3")
        port = cur.fetchone()
        con.commit()
        cur.close()
        con.close()

        if port is None:
            return False
        else:
            return port[0]

    def delete_port_from_db(self):
        database_host = 'database'
        con = psycopg2.connect(database='postgres', user='postgres', password='postgres', host=database_host,
                               port='5432')
        cur = con.cursor()
        cur.execute("DELETE FROM mpi_port_information WHERE port_type=3")
        con.commit()
        cur.close()
        con.close()

    def connect_to_analysis_server(self, port=-1):
        if port == -1:
            #port = self.get_port_from_file()
            port = self.get_port_from_db()
            while port is False:
                time.sleep(0.5)
                port = self.get_port_from_db()
                logger.debug(f"Waiting for the connection port")
            print(f"Port: {port}")
        logger.debug(f"Trying to connect to {port}")
        self.intercommunicator = self.mpi_world.Connect(port, MPI.INFO_NULL, root=0)
        print(self.intercommunicator)
        logger.debug(f"Successfully connect to {port} with rank {self.intercommunicator.Get_rank()}")
        logger.debug(f"Established intercommunicator: {self.intercommunicator}")
        self.delete_port_from_db()
        return True

    def get_port_from_file(self):
        logger.debug(f"Getting port details")
        fport_path = "/tmp/intercommport_in.txt"
        while not os.path.exists(fport_path):
            time.sleep(1)
            logger.debug(f"Waiting for port details")
        fport = open(fport_path, "r")
        port = fport.read()
        fport.close()
        logger.debug(f"Got Port: '{port}' from file")
        return port

    def send_data(self, data, dest=1, tag=999):
        if self.intercommunicator is not None:
            logger.debug(f"Trying to send data to analysis server")
            self.intercommunicator.send(data, dest, tag)
            logger.debug(f"Successfully sent data to anaylsis server")
        else:
            logger.debug(f"Intercommunicator is None")
            # TODO: implement exceptions ?

    def get_data(self, source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG):
        if self.intercommunicator is not None:
            logger.debug(f"Waiting for data from analysis server")
            received_data = self.intercommunicator.recv(source=source, tag=tag)
            logger.debug(f"Successfully received data from analysis server")
            return received_data
        else:
            # TODO: implement exceptions ?
            logger.debug(f"Intercommunicator is None")

    def call_plugin_function(self, id, func, *args):
        data = {'plugin_id': id, 'func_name': func, 'args': args}
        self.send_data(data, dest=1, tag=100)
        response = self.get_data()
        if response['success'] is True:
            return response['function_return']
        else:
            return response
