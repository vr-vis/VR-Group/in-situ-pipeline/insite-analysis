import json
import time
import os
from datetime import datetime

class perf:
    queue = []

    @staticmethod
    def start_func(name):
        perf.queue.append({
            'name': name,
            "cat": "PERF",
            "ph": "B",
            "ts": time.time() * 1000000,
            "pid": 1,
            "tid": 1
        })

    @staticmethod
    def start_func_immediate(name, pid=1, tid=1):
        with open('data.json','a+') as f:
            f.write("\n,")
            f.write(json.dumps({
            'name': name,
            "cat": "PERF",
            "ph": "B",
            "ts": time.time() * 1000000,
            "pid": pid,
            "tid": tid
        }))


    @staticmethod
    def write_header():
        with open('data.json','w') as f:
            f.write("[{}")

    @staticmethod
    def write_footer():
        with open('data.json','a+') as f:
            f.write("]")
        now = datetime.now()
        os.rename('data.json','data_'+ now.strftime("%d_%m_%H_%M") +'.json')

    @staticmethod
    def end_func_immediate(name, pid=1, tid=1):
        with open('data.json','a+') as f:
            f.write("\n,")
            f.write(json.dumps({
            'name': name,
            "cat": "PERF",
            "ph": "E",
            "ts": time.time() * 1000000,
            "pid": pid,
            "tid": tid
        }))

    @staticmethod
    def end_func(name):
        perf.queue.append({
            'name': name,
            "cat": "PERF",
            "ph": "E",
            "ts": time.time() * 1000000,
            "pid": 1,
            "tid": 1
        })

    @staticmethod
    def write_timings():
        with open('data'+str(time.time())+'.json','w') as f:
            json.dump(perf.queue,f)

    @staticmethod
    def event_immediate(name, pid=1, tid=1):
        with open('data.json','a+') as f:
            f.write("\n,")
            f.write(json.dumps({
            'name': name,
            "ph": "i",
            "ts": time.time() * 1000000,
            "pid": pid,
            "tid": tid,
            "s":"g"
        }))
    @staticmethod
    def event(name):
        perf.queue.append({
            'name': name,
            "ph": "i",
            "ts": time.time() * 1000000,
            "pid": 1,
            "tid": 1,
            "s":"g"
        })