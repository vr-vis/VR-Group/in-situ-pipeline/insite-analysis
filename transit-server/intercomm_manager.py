from mpi4py import MPI
import logging
from enum import IntEnum
import time
import psycopg2


logger = logging.getLogger(__name__)


class Tag(IntEnum):
    FUNCTION_CALL = 100
    GET_PLUGINS = 101


class IntercommManager():
    intercommunicator = None
    request = None
    plugin_manager = None

    def __init__(self):
        pass

    def save_port_in_db(self, port_type, port):
        print("Writing to DB")
        database_host = 'database'
        con = psycopg2.connect(database='postgres', user='postgres', password='postgres', host=database_host,
                               port='5432')
        cur = con.cursor()
        cur.execute("DELETE FROM mpi_port_information WHERE port_type =%s",[port_type])
        cur.execute("INSERT INTO mpi_port_information(port_type,port) VALUES (%s,%s)", (port_type, port))
        con.commit()
        cur.close()
        con.close()

    def create_intercomm_connection(self):
        if MPI.COMM_WORLD.Get_rank() == 0:
            logger.debug(f"Rank 0 is trying to open MPI port")
            intercomm_port = MPI.Open_port(MPI.INFO_NULL)
            logger.debug(f"Sucessfully opened MPI port {intercomm_port}")
            logger.debug(f"Writing port to filesystem")
            intercommport_path = "/tmp/intercommport_in.txt"
            intercommport_file = open(intercommport_path, "w")
            intercommport_file.write(intercomm_port)
            intercommport_file.close()
            self.save_port_in_db(3,intercomm_port)
        else:
            intercomm_port = None

        logger.debug(f"Broadcasting port information")
        intercomm_port = MPI.COMM_WORLD.bcast(intercomm_port, root=0)
        logger.debug(f"Successfully broadcastet port information")
        logger.debug(f"Rank {MPI.COMM_WORLD.Get_rank()} waiting to accept a connection on {intercomm_port}")
        self.intercommunicator = MPI.COMM_WORLD.Accept(intercomm_port, MPI.INFO_NULL, root=0)
        logger.debug(f"Client successfully connected to MPI port on Rank {MPI.COMM_WORLD.Get_rank()}")

    def check_for_intercomm(self):
        success = None
        status_resp = MPI.Status()
        if self.request is not None:
            success, data = self.request.test(status=status_resp)

        if self.request is None or success is True:
            self.request = self.intercommunicator.irecv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG)

            if success is True:
                stat = MPI.Status()
                self.handle_intercomm_data(status_resp, data)

        else:
            pass
            # time.sleep(0.5)
            # logger.debug(f"No data available: Waiting for 0.5s")

    def handle_intercomm_data(self, status, data):
        received_tag = status.Get_tag()
        logger.debug(f"Received the following tag {received_tag} with data: {data}")
        print(f"func_all tag: {Tag.FUNCTION_CALL}")
        if int(received_tag) == Tag.FUNCTION_CALL:
            self.handle_function_call(data)
        elif int(received_tag) == Tag.GET_PLUGINS:
            self.get_plugin_infos()

    def handle_function_call(self, data):
        plugin_id = data['plugin_id']
        func = data['func_name']
        func_args = data['args']
        logger.debug(f"Trying to handle function call to plugin_id: {plugin_id} func_name:{func} and args: {func_args}")

        response = self.plugin_manager.call_function_on_plugin(plugin_id, func, *func_args)
        response_data = {}
        response_data['success'] = True
        response_data['function_return'] = response
        logger.debug(f"Sending answer to access node")
        self.intercommunicator.send(response_data, dest=0, tag=111)

    def get_plugin_infos(self):
        response = []

        print(self.plugin_manager.get_plugins())
        for id, plugin_obj in enumerate(self.plugin_manager.get_plugins()):
            plugin = {}
            plugin['id'] = id
            plugin['name'] = plugin_obj.name
            plugin['short_description'] = self.plugin_manager.get_short_description(id)
            plugin['activated'] = self.plugin_manager.get_plugin_activated(id)['active']
            response.append(plugin)
        print(response)
        self.intercommunicator.send(response, dest=0, tag=111)
    def get_plugin_params(self, data):
        plugin_id = data['plugin_id']
        params =  self.plugin_manager.get_plugin_params(plugin_id)
        self.intercommunicator.send(params, dest = 0, tag = 111)