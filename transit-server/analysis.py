import sys
import os
from mpi4py import MPI
import numpy as np
from plugin_management import PluginManagement
import time
from intercomm_manager import IntercommManager
import psycopg2
import logging
import multiprocessing
from mpi4py.futures import MPIPoolExecutor
from performance_measure import perf

class AppFilter(logging.Filter):
    rank = 0

    def __init__(self):
        self.rank = MPI.COMM_WORLD.Get_rank()

    def filter(self, record):
        record.mpi_rank = self.rank
        return True


ch = logging.StreamHandler(sys.stdout)
ch.setLevel(1)
ch.setFormatter(logging.Formatter('[%(filename)20s:%(lineno)4s - %(funcName)35s() - %(mpi_rank)s] %(message)s'))
ch.addFilter(AppFilter())
root = logging.getLogger()
root.setLevel(logging.DEBUG)
root.addHandler(ch)

logging.getLogger('yapsy').setLevel(logging.ERROR)
logging.getLogger('matplotlib').setLevel(logging.WARNING)

logger = logging.getLogger(__name__)


neuron_one = np.empty(shape=(1, 1))

np.set_printoptions(precision=4)

plugin_manager = PluginManagement()

comm = MPI.COMM_WORLD


intercomm = None

intercomm_to_access_node = IntercommManager()

processing_times = []

buffer_pos = np.zeros(dtype='i', shape=3130)


def prepare_shared_memory():
    logger.debug("Setting up shared memory")
    size = 3125

    itemsize = MPI.DOUBLE.Get_size()

    if comm.Get_rank() == 0:
        buffer_size = size * itemsize * 500
    else:
        buffer_size = 0

    win = MPI.Win.Allocate_shared(buffer_size, itemsize, comm=comm)

    shared_buff, itemsize = win.Shared_query(0)

    data_buffer = np.ndarray(buffer=shared_buff, dtype='d', shape=(size, 500))

    logger.debug("Waiting on comm Barrier")
    comm.Barrier()
    logger.debug("Comm Barrier cleared")
    return data_buffer


def get_port_from_db():
    database_host = 'database'
    con = psycopg2.connect(database='postgres', user='postgres', password='postgres', host=database_host, port='5432')
    cur = con.cursor()
    cur.execute("SELECT port FROM mpi_port_information WHERE port_type = 1")
    port = cur.fetchone()
    con.commit()
    cur.close()
    con.close()
    return port


def init_conn_to_transit_server(number_of_neurons):
    # Generally speaking we always want the data for all neurons


    ## Init connection
    comm = MPI.COMM_WORLD

    # The in-transit server write its port information to anaport_in.txt
    mpi_port_path = "/tmp/anaport_in.txt"
    while not os.path.exists(mpi_port_path):
        time.sleep(0.5)

    if os.path.isfile(mpi_port_path):  
        analysis_port = open(mpi_port_path, "r").read()
        analysis_port.close()
        communicator_analysis_server = comm.Connect(analysis_port, MPI.INFO_NULL, root=0)
    
    if comm.Get_rank() == 0:
        # The transit server expects this packet
        communicator_analysis_server.send(number_of_neurons, dest=0, tag=111)

        process_spikes(communicator_analysis_server)

def process_spikes(comm_analysis):
    # global request
    status_ = MPI.Status()
    counter = 0
    request = None
    success = None
    call = 0
    
    data_from_node_two = []
    count_node_one = 0

    time1_recv = time.time()
    data_buffer = prepare_shared_memory()
    while True:

        if request is not None:
            success, data2 = request.test(status=status_)

        if success is True:
            perf.end_func_immediate("recv_spikes")

        if request is None or success is True:
            request = comm_analysis.irecv(800000, source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG)
            perf.start_func_immediate("recv_spikes")

        if success is True:
            call += 1
            if call == 1:
                print(len(data2))
            # The transit server send a packet with tag 10 to indicate the end
            if status_.tag == 10:
                print(f"received last data package number: {counter + 1} from rank: {status_.source}")
                print(f"simulation ended, no new data to receive.")
                print("finished writing logs")
                perf.end_func_immediate("recv_spikes")
                #Send 999 to local MPI world to indicate the end for the plugins
                comm.send(0,dest=1,tag=999)
            elif status_.tag == 11:
                write_spikes_to_shared_memory(data_buffer, data2, status_)
                if status_.source == 2:
                    data_from_node_two = data2.copy()

                if status_.source == 1:
                    print("Total time : %.1f ms" % (1000 * (time.time() - time1_recv)))
                    count_node_one += data2[0].size
                    print(count_node_one)
                    data = data2 + data_from_node_two
                    perf.start_func_immediate("send_spikes_to_analysis")
                    comm.send(data, dest=1, tag=910)
                    perf.end_func_immediate("send_spikes_to_analysis")
                    perf.end_func_immediate("recv_spike_idle")
                    perf.start_func_immediate("recv_spike_idle")
                    time1_recv = time.time()
            perf.end_func_immediate("recv_spikes")

def write_spikes_to_shared_memory(shared_memory, neurons, status):
    perf.start_func_immediate("spikes_to_shared_mem")
    global buffer_pos
    time1 = time.time()
    number_of_neurons_in_pkg = len(neurons)
    if status.source == 1:
        offset = 0
    else:
        offset = 1563

    buffer_lpos = buffer_pos[offset:offset + number_of_neurons_in_pkg].tolist()
    new_sizes = [neuron.size for neuron in neurons]

    for r in range(number_of_neurons_in_pkg):

        if buffer_lpos[r] + neurons[r].size > shared_memory[r + offset].size:

            room_left_in_buffer = shared_memory[r + offset].size - buffer_lpos[r]
            shared_memory[r + offset][buffer_lpos[r]:shared_memory[r + offset].size] = neurons[r][: room_left_in_buffer]
            shared_memory[r + offset][:neurons[r].size - room_left_in_buffer] = neurons[r][room_left_in_buffer:]

        else:

            shared_memory[r + offset][buffer_lpos[r]:buffer_lpos[r] + neurons[r].size] = neurons[r]

    buffer_pos[offset:offset + number_of_neurons_in_pkg] = (buffer_pos[
                                                            offset:offset + number_of_neurons_in_pkg] + np.array(
        new_sizes)) % 500

    perf.end_func_immediate("spikes_to_shared_mem")

def process_data():
    data_buffer = prepare_shared_memory()
    request = None
    status_ = MPI.Status()
    success = None
    finished = False
    while True:
        intercomm_to_access_node.check_for_intercomm()

        if finished:
            continue

        if request is not None:
            success, delta_data = request.test(status=status_)

        if request is None or success is True:
            perf.start_func_immediate("process_data_irecv")
            request = comm.irecv(800000, source=0, tag=MPI.ANY_TAG)
            perf.end_func_immediate("process_data_irecv")

        if status_.tag == 999:
            for plugin in plugin_manager.plugin_objects:
                plugin.plugin_object.on_end()
            print("process data wrote perf logs")
            perf.write_footer()
            finished = True

        if success is True:
            perf.start_func_immediate("pluginDispatch")
            jobs = []
            time1 = time.time()
            for plugin in plugin_manager.plugin_objects:
                if plugin.plugin_object.is_activated:
                    job = multiprocessing.Process(target=plugin.plugin_object.on_data, args=(data_buffer, delta_data))
                    jobs.append(job)
                    job.start()

            for job in jobs:
                job.join()
            print(f"Plugins took time : %.1f ms" % (1000 * (time.time() - time1)))
            perf.end_func_immediate("pluginDispatch")


if __name__ == "__main__":

    intercomm_to_access_node.create_intercomm_connection()
    intercomm_to_access_node.plugin_manager = plugin_manager
    init_conn_to_transit_server(int(sys.argv[1]))

    rank = MPI.COMM_WORLD.Get_rank()

    if rank == 0:
        pass

    if rank == 1:
        perf.write_header()
        plugin_manager.load_plugins()
        plugin_manager.print_loaded_plugins()
        for plugin in plugin_manager.get_plugins():
            plugin.plugin_object.on_init()
        process_data()
