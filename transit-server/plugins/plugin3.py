import plugins.BasePlugin as BasePlugin
import numpy as np
from elephant.statistics import mean_firing_rate
import multiprocessing
from performance_measure import perf
import logging
from datetime import datetime

logger = logging.getLogger(__name__)


class MyPlugin(BasePlugin.BasePlugin):
    mfr_data = multiprocessing.Array('d', 3125, lock=False)
    max = multiprocessing.Value('d', 0)
    time_range = 500
    start_id = 1
    end_id = 5
    time = datetime.now().strftime("%d_%m_%H_%M")

    def on_init(self):
        self.activate()

    def print_name(self):
        print(f"MyPlugin3 activation status= {self.is_activated}")

    def on_data(self, data, delta_data):
        return
        perf.start_func_immediate("P3::onData")
        with open('p3data_'+self.time+'.txt','a+') as f:
            for idx, neuron in enumerate(delta_data):
                self.mfr_data[idx] += neuron.size
                for spike in neuron:
                    f.write(str(int(idx))+" "+str(int(spike))+"\n")

        perf.end_func_immediate("P3::onData")
        return

    def get_data(self):
        # Prepare an empty dict for the response
        result = {}
        result['time_range'] = self.time_range
        result['timestamp'] = self.max.value

        # Prepare an empty dict for the neurons mfrs
        result['mfr'] = {}

        # Insert the mfr of the requested neuorons
        for i in range(self.start_id, self.end_id):
            result['mfr'][i] = self.mfr_data[i]

        # Return the result dict
        return result

    def on_end(self):
        logger.debug("P3 on_end")
        return