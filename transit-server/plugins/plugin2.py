import plugins.BasePlugin as BasePlugin
import numpy as np
import requests
import logging
import time
import multiprocessing
from performance_measure import perf

logger = logging.getLogger(__name__)


class MyPlugin2(BasePlugin.BasePlugin):
    i = multiprocessing.Value('i', 0)
    last_min = multiprocessing.Value('d', -1)
    time_avg = multiprocessing.Value('d', 0)
    time_resolution = 0.3
    space_resolution = 20
    populations = []
    population_index = []
    population_list = []
    binning_sets = []
    binning_list = []
    bin_count = 40
    size_ringbuffer = 200
    time_res = 1.0

    ## New implementation
    no_populations = 2
    no_timebins = 15000
    no_spatialbins = bin_count * bin_count
    time_resolution = 1.0

    active_tbins = np.frombuffer(multiprocessing.Array('d', [False] * no_timebins, lock=False))
    finished_tbins = np.frombuffer(multiprocessing.Array('d', [False] * no_timebins, lock=False))

    lower_time_limits = np.frombuffer(multiprocessing.Array('d', [-1] * no_timebins, lock=False))

    min_spiketime = multiprocessing.Value('d', 0)

    raw_bin_memories = multiprocessing.Array('d', no_populations * no_spatialbins * no_timebins, lock=False)
    unshaped_bin_buffer = np.frombuffer(raw_bin_memories)
    bins = unshaped_bin_buffer.reshape((no_populations, no_timebins, no_spatialbins))

    current_max = -1

    def on_data(self, data, delta_data):
        perf.start_func_immediate("P2::onData")
        sizes = []
        for spikes in delta_data:
            sizes.append(spikes.size)

        indices_population = []
        indices_spatialbins = []

        for idx, size in enumerate(sizes):
            indices_population.extend([self.pop_lut[idx]] * size)
            indices_spatialbins.extend([self.bin_lut[idx]] * size)

        spikes_flattened = np.concatenate([spikes for spikes in delta_data])
        timestep_number = np.floor_divide(spikes_flattened, self.time_resolution)
        time_bins = np.remainder(timestep_number, self.no_timebins)

        self.min_spiketime.value = np.min(spikes_flattened)

        unique_bins, unique_bins_indices = np.unique(time_bins, return_index=True)

        self.lower_time_limits[unique_bins.astype(int)] = self.time_resolution * timestep_number[unique_bins_indices]

        np.add.at(self.bins, (indices_population, time_bins.astype(int), indices_spatialbins), 1)

        indices_finished_bins = np.where(
            (self.lower_time_limits < self.min_spiketime.value) & (self.lower_time_limits != -1))

        self.active_tbins[indices_finished_bins] = False
        self.finished_tbins[indices_finished_bins] = True
        perf.end_func_immediate("P2::onData")
        return

    # Returns the bins and the corresponding timestamps from bins that are finished AND in the given timerange
    # the returned bins include all populations
    def get_bins(self, from_time, to_time):
        if from_time < 0:
            from_time = 0

        if to_time < 0:
            to_time = 0

        # return the indices of timebins that fulfill: from_time <= time <= to_time
        bins_in_timerange = np.where(
            (self.lower_time_limits >= from_time) &
            (self.lower_time_limits + self.time_resolution <= to_time))

        bins_finished = np.where(self.finished_tbins == True)

        # Get the intersection to only return bins that are already finished and can therefore be used for further processing
        # and that are within the given timerange
        bins_finished_in_timerange = np.intersect1d(bins_in_timerange, bins_finished)

        # returns timestamps, bins.. bins[:, bins_in_timerange, :] means that it returns all spatial bins and all
        # population within the given timerange
        return self.lower_time_limits[bins_finished_in_timerange], self.bins[:, bins_finished_in_timerange, :]

    def get_bins_json(self, from_time, to_time):
        list_of_mfrs = []

        timestamps, bins = self.get_bins(from_time, to_time)

        # return empty list if no timestamps were returned
        if len(timestamps) == 0:
            return {"error":"len timestamps = 0"}

        for pop_id in range(0, self.no_populations):
            for timestamp, spike_sum in zip(timestamps, bins[pop_id]):
                list_of_mfrs.append({
                    "timestep": float(timestamp),
                    "population": pop_id,
                    "data": (100*spike_sum / self.time_resolution)
                })

        return list_of_mfrs

    def on_init(self):
        perf.start_func_immediate("P2::onInit")
        perf.end_func_immediate("P2::onInit")
        self.activate()
        logger.debug("setting up neuron information")
        neurons = get_neuron_positions()
        self.populations, self.population_index = get_neurons_by_population(neurons)
        logger.debug(f"Got {len(self.population_index)} populations with the following numbers of neurons:")
        for pop in self.populations:
            logger.debug(f"{len(pop)} neurons")

        for pop in self.populations:
            bin_set = get_binned_pos_set(pop, self.bin_count, get_extents(pop))
            self.binning_sets.append(bin_set)

        for pop in self.populations:
            bin_list = get_binned_pos_list(pop, self.bin_count, get_extents(pop))
            self.binning_list.append(bin_list)

        self.build_population_lookup(neurons, self.population_index)
        self.build_bin_lookup(neurons, get_extents(neurons))

        for i in range(len(self.population_index)):
            logger.debug(f"Population {i} has {self.pop_lut.count(i)} no neurons")

        for i in range(self.bin_count * self.bin_count):
            logger.debug(f"Bin {i} has {self.bin_lut.count(i)} no neurons")

        perf.end_func_immediate("P2::onInit")
        return
        self.counting_buffer.fill(0)
        self.activate()
        logger.debug("on init called")

    def build_bin_lookup(self, neurons, max_value):
        self.bin_lut = []
        for neuron in neurons:
            self.bin_lut.append(position_to_bin(neuron[2], neuron[3], self.bin_count, max_value))

    def build_population_lookup(self, neurons, population_indicies):
        self.pop_lut = []
        self.no_populations = len(population_indicies)
        for neuron in neurons:
            self.pop_lut.append(population_indicies.index(neuron[1]))

    def on_end(self):
        pass
    #    np.savetxt("p2.out",self.bins[0])
    #    with open('p2data.json','w') as f:
    #        for i,row in enumerate(self.bins[0]):
    #            for j,column in enumerate(row):
    #                if column != 0:
    #                    f.write(f"{j%self.bin_count} {j//self.bin_count} {i} {column}\n")
    #    return

    def print_name(self):
        print(f"MyPlugin2 activation status= {self.is_activated}")

    def get_data(self, params=None):
        perf.start_func_immediate("P2::getData")
        to_time = self.min_spiketime.value
        from_time = 0

        if params:
            if "to_time" in params:
                to_time = float(params["to_time"])
            if "from_time" in params:
                from_time = float(params["from_time"])
            if "range" in params:
                to_time = self.min_spiketime.value
                from_time = self.min_spiketime.value - float(params["range"])
            if "from_time" in params and "to_time" not in params:
                from_time = float(params["from_time"])
                to_time = from_time + 100
        logger.debug(f"requesting data for: {from_time} to {to_time}")
        ret = self.get_bins_json(from_time, to_time)
        perf.end_func_immediate("P2::getData")
        return ret


    def get_binned_pos_set(positions, bin_count, max_value):
        bin_set = {}

        bins = np.linspace(-max_value, max_value + 0.001, bin_count + 1)
        for i, neuron in enumerate(positions):
            digitize_x = np.digitize(neuron[2].astype(float), bins)
            digitize_y = np.digitize(neuron[3].astype(float), bins)

            bin_index = (digitize_x - 1) + ((digitize_y - 1) * (bin_count))

            if bin_index in bin_set:
                bin_set[bin_index].append(i)
            else:
                bin_set[bin_index] = [i]

        return bin_set


    def get_binned_pos_list(positions, bin_count, max_value):
        bin_list = [None] * 3125

        bins = np.linspace(-max_value, max_value + 0.001, bin_count + 1)
        for neuron in positions:
            digitize_x = np.digitize(neuron[2].astype(float), bins)
            digitize_y = np.digitize(neuron[3].astype(float), bins)

            bin_list[int(neuron[0]) - 1] = (digitize_x - 1) + ((digitize_y - 1) * (bin_count))
        return bin_list


    def position_to_bin(pos_x, pos_y, bin_count, max_value):
        bins = np.linspace(-max_value, max_value + 0.001, bin_count + 1)
        digitize_x = np.digitize(pos_x.astype(float), bins)
        digitize_y = np.digitize(pos_y.astype(float), bins)

        return (digitize_x - 1) + ((digitize_y - 1) * bin_count)

    def get_neurons_by_population(neurons):
        populations = []
        population_index = []

        for population_id in np.unique(neurons[:, 1]):
            populations.append(neurons[(neurons[:, 1] == population_id)])
            population_index.append(population_id)

        return populations, population_index


    def get_neuron_population_list(neurons):
        population_list = []
        for neuron in neurons:
            population_list.append(neuron[1])

        return population_list


    def get_neuron_positions():
        time.sleep(2)
        r = requests.get('http://localhost:8080/nest/neuron_properties')
        neuron_information = np.array(
            [[data["gid"]] + [data['properties']['population_id']] + data['properties']['position'] for data in r.json()])
        neuron_information = neuron_information[np.argsort(neuron_information[:, 0])]
        return neuron_information


    def get_extents(neuron_positions):
        # gid, population, x, y
        _, _, max_x, max_y = np.max(neuron_positions, axis=0)
        _, _, min_x, min_y = np.min(neuron_positions, axis=0)

        assert (max_x == max_y == -min_x == -min_y)
        return max_x


    def get_min_max_value(delta_package):
        concatenated_pkg = np.concatenate([neuron for neuron in delta_package])
        if concatenated_pkg.size > 0:
            return np.min(concatenated_pkg), np.max(concatenated_pkg)
        else:
            return 0, 0
