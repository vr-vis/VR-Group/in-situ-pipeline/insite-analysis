import plugins.BasePlugin as BasePlugin
import numpy as np
from elephant.statistics import mean_firing_rate
import multiprocessing
from elephant.statistics import isi,cv

class MyPlugin(BasePlugin.BasePlugin):
    isi_data = multiprocessing.Array('d', 3125, lock=False)
    cv_data = multiprocessing.Array('d', 3125, lock=False)
    max = multiprocessing.Value('d', 0)
    start_id = 20
    end_id = 30

    def on_init(self):
        self.activate()

    def print_name(self):
        print(f"MyPlugin1 activation status= {self.is_activated}")

    def on_data(self, data, delta_data):
        for idx, neuron in enumerate(data[self.start_id:self.end_id]):
            if np.count_nonzero(neuron) > 0:
                non_zero = neuron[np.nonzero(neuron)]
                isi_data = isi(non_zero)
                self.isi_data[idx+self.start_id] = np.average(isi_data)
                self.cv_data[idx+self.start_id] = cv(isi_data)
                if idx == 0:
                    print(isi_data)
                    print(self.cv_data[0])
        print(self.isi_data[0])
        return

    def get_data(self, params=None):
        # Prepare an empty dict for the response
        result = {}

        # Prepare an empty dict for the neurons mfrs
        result['data'] = {}

        # Insert the mfr of the requested neuorons
        for i in range(self.start_id, self.end_id):
            result['data'][str(i)] = {}
            result['data'][str(i)]['isi'] = self.isi_data[i]
            result['data'][str(i)]['cv'] = self.cv_data[i]

        # Return the result dict
        return result

    def on_end(self):
        return