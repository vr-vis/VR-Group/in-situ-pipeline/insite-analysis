#ifndef INSITEMODULE__RECORDING_BACKEND_INSITE_MPI_H_
#define INSITEMODULE__RECORDING_BACKEND_INSITE_MPI_H_

#include "node_collection.h"
#include "nest_types.h"
#include "recording_backend.h"
#include "mpi.h"
#include "data_storage.hpp"

namespace insite {
  class RecordingBackendInsiteMPI : public nest::RecordingBackend{

   public:
    RecordingBackendInsiteMPI();

    ~RecordingBackendInsiteMPI() throw() override;

    void initialize() override;
    void finalize() override;

    void enroll(const nest::RecordingDevice& device,
                const DictionaryDatum& params) override;

    void disenroll(const nest::RecordingDevice& device) override;

    void set_value_names(const nest::RecordingDevice& device,
                         const std::vector<Name>& double_value_names,
                         const std::vector<Name>& long_value_names) override;

    void prepare() override;

    void cleanup() override;

    void pre_run_hook() override;

    void post_run_hook() override;

    void post_step_hook() override;

    void write(const nest::RecordingDevice& device, const nest::Event& event,
               const std::vector<double>& double_values,
               const std::vector<long>& long_values) override;

    void set_status(const DictionaryDatum& params) override;

    void get_status(DictionaryDatum& params) const override;

    void check_device_status(const DictionaryDatum& params) const override;

    void get_device_defaults(DictionaryDatum& params) const override;

    void get_device_status(const nest::RecordingDevice& device,
                           DictionaryDatum& params) const override;

   private:

    int mpi_world_rank_;

    int max_spikes_per_step = -1;
    MPI_Comm intercomm_to_transit_server_;

    struct SpikeDouble {
      double gid;
      double simulation_time;
    };

    std::vector<SpikeDouble> spike_data_;

    void send_data_to_transit();
    void connect_to_transit_server(const std::string& port_name);
    std::string get_port_from_database();
  };
}



#endif //INSITEMODULE__RECORDING_BACKEND_INSITE_MPI_H_
