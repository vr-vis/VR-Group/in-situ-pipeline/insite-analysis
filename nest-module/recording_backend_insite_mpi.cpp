//
// Created by marcl on 08.04.20.
//

#include <pqxx/connection>
#include <pqxx/transaction>
#include "recording_backend_insite_mpi.h"
#include "recording_device.h"
#include "kernel_manager.h"

namespace insite {

//TODO: Implement Constructor
RecordingBackendInsiteMPI::RecordingBackendInsiteMPI() {
  MPI_Comm_rank(MPI_COMM_WORLD,&mpi_world_rank_);
  create_spike_mpi_datatype();
}

RecordingBackendInsiteMPI::~RecordingBackendInsiteMPI() throw() {} //empty for now

void RecordingBackendInsiteMPI::initialize() {
  std::cout << "RecordingBackendInsiteMPI::initialize()\n";
  auto port_name = get_port_from_database();
  connect_to_transit_server(port_name);
}

void RecordingBackendInsiteMPI::finalize() {
  std::cout << "RecordingBackendInsiteMPI::finalize()\n";
}

void RecordingBackendInsiteMPI::enroll(const nest::RecordingDevice &device, const DictionaryDatum &params) {
  std::cout << "RecordingBackendInsiteMPI::enroll(" << device.get_label() << ")\n";
}

void RecordingBackendInsiteMPI::disenroll(const nest::RecordingDevice &device) {
  std::cout << "RecordingBackendInsiteMPI::disenroll(" << device.get_label() << ")\n";
}

void RecordingBackendInsiteMPI::set_value_names(
    const nest::RecordingDevice &device,
    const std::vector<Name> &double_value_names,
    const std::vector<Name> &long_value_names) {
  std::cout << "RecordingBackendInsiteMPI::set_value_names()\n";

}

void RecordingBackendInsiteMPI::prepare() {}

void RecordingBackendInsiteMPI::cleanup() {
  std::cout << "RecordingBackendInsiteMPI::cleanup()\n";
}

void RecordingBackendInsiteMPI::pre_run_hook() {
  std::cout << "RecordingBackendInsiteMPI::pre_run_hook()\n";
}

void RecordingBackendInsiteMPI::post_run_hook() {
  std::cout << "RecordingBackendInsiteMPI::post_run_hook()\n";

  //Send end message to in-transit server
  double last[2] = {0,0};
  MPI_Send(last, 2, MPI_DOUBLE, 0, 113, intercomm_to_transit_server_);

}

void RecordingBackendInsiteMPI::post_step_hook() {

}

void RecordingBackendInsiteMPI::write(const nest::RecordingDevice& device,
                                   const nest::Event& event,
                                   const std::vector<double>& double_values,
                                   const std::vector<long>& long_values) {
  if (device.get_type() == nest::RecordingDevice::SPIKE_RECORDER) {


    const auto sender_gid = event.get_sender_node_id();
    const auto time_stamp = event.get_stamp().get_ms();

    const SpikeDouble current_spike{static_cast<double>(sender_gid),time_stamp};
    spike_data_.push_back(current_spike);


    if (spike_data_.size() == 500) {
      send_data_to_transit();
      spike_data_.clear();
      assert(spike_data_.size() == 0);
    }

  }
}

void RecordingBackendInsiteMPI::set_status(const DictionaryDatum& params) {
  std::cout << "RecordingBackendInsiteMPI::set_status()\n";
}

void RecordingBackendInsiteMPI::get_status(DictionaryDatum& params) const {
  std::cout << "RecordingBackendInsiteMPI::get_status()\n";
}

void RecordingBackendInsiteMPI::check_device_status(
    const DictionaryDatum& params) const {
  std::cout << "RecordingBackendInsiteMPI::check_device_status()\n";
}

void RecordingBackendInsiteMPI::get_device_defaults(
    DictionaryDatum& params) const {
  std::cout << "RecordingBackendInsiteMPI::get_device_defaults()\n";
}

void RecordingBackendInsiteMPI::get_device_status(
    const nest::RecordingDevice& device, DictionaryDatum& params) const {
  std::cout << "RecordingBackendInsiteMPI::get_device_status()\n";
}

void RecordingBackendInsiteMPI::connect_to_transit_server(const std::string &port_name) {
  auto error = MPI_Comm_connect(port_name.c_str(),MPI_INFO_NULL,0,MPI_COMM_WORLD,&intercomm_to_transit_server_);

  if(error == MPI_SUCCESS)
  {
    std::cout << "[Rank: " << mpi_world_rank_ << "] MPI_Comm_connect successfully connected to: " << port_name << std::endl;
  }
  else
    std::cout << "MPI_Comm_connect error code: " << error << std::endl;

}

void RecordingBackendInsiteMPI::send_data_to_transit() {
  std::cout << std::endl <<  "Sending pkg number "  << pkg_count << " from node " << mpi_world_rank_ << std::endl;
  MPI_Send(spike_data_.data(), spike_data_.size()*2, MPI_DOUBLE, 0, 14, intercomm_to_transit_server_);
  pkg_count++;
}

std::string RecordingBackendInsiteMPI::get_port_from_database() {
  pqxx::connection connection_("postgresql://postgres@database");
  pqxx::work txn(connection_);

    pqxx::result result;
    while(true)
    {
      result = txn.exec("SELECT port FROM mpi_port_information WHERE port_type=2");
      if(result.size() == 1)
        break;
      else
      {
        std::cout << "Waiting for port details" << std::endl;
        sleep(1);
      }
    }
  auto row = result[0];
  auto port_name = row[0].as<std::string>();
  return port_name;
}


} //namepsace insite end